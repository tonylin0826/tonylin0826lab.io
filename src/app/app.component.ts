import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatTooltip } from '@angular/material/tooltip';
import { faGithub, faLinkedin, faNpm, faStackOverflow } from '@fortawesome/free-brands-svg-icons';
import { faTerminal } from '@fortawesome/free-solid-svg-icons';
import { Terminal } from 'xterm';
import { FitAddon } from 'xterm-addon-fit';
import { WebLinksAddon } from 'xterm-addon-web-links';

class MyTerm extends Terminal {

  private textBuffer = '';

  private fitAddon = new FitAddon();

  constructor() {
    super();

    this.loadAddon(new WebLinksAddon());
    this.loadAddon(this.fitAddon);
    // this.setOption('padding', 5);

    this.onKey((e: { key: string, domEvent: KeyboardEvent }) => {
      const ev = e.domEvent;
      const printable = !ev.altKey && !ev.ctrlKey && !ev.metaKey;

      if (ev.key === 'ArrowUp' || ev.key === 'ArrowDown') {
        return;
      }

      if (ev.key === 'Tab') {
        const options = ['help', 'profile', 'resume', 'portfolio', 'links'];
        for (const opt of options) {
          if (this.textBuffer !== '' && opt.startsWith(this.textBuffer)) {
            this.write(opt.substr(this.textBuffer.length));
            this.textBuffer = opt;
            break;
          }
        }
        return;
      }

      if (ev.key === 'Enter') {
        this.process();
        this.prompt();
      } else if (ev.key === 'Backspace') {
        // Do not delete the prompt
        if ((this as any)._core.buffer.x > 2) {
          this.write('\b \b');
          this._putBackspace();
        }
      } else if (printable) {
        this.write(e.key);
        this._putToBuffer(e.key);
      }
    });
  }

  init(): void {
    this.fitAddon.fit();
    this.write(`Hello, my name is Tony. Type 'help' to see more`);
    this._putToBuffer('help');
    this.process();
    this.prompt();
  }

  prompt(): void {
    this.write('\r\n$ ');
  }

  process(): void {
    if (this.textBuffer === '') {
      return;
    }

    this.write('\r\n');
    switch (this.textBuffer) {
      case 'help':
        this.writeln(`Support command:`);
        this.writeln(` - profile\tshow profile`);
        this.writeln(` - portfolio\tshow portfolio`);
        this.writeln(` - links\tshow links`);
        this.writeln(` - resume\topen resume`);
        this.writeln(` - help\t\tshow usage`);
        break;
      case 'profile':
        this.writeln(`Hi, I'm Tony. I've been a full-stack engineer for more than 4 years.`);
        this.writeln('');
        this.writeln(`During my time in Ardomus Networks, a subsidiary of Zyxel, I'm responsible for leading a 6-person team to develop IoT applications. One project worth mentioning is the HES(Head-end System), a national project we develop as a part of Taiwan's AMI(Advanced Metering Infrastructure) for Taiwan-Power to manage over 70k of their meters and FAN modules. I and my team put a lot of effort into making this system distributed and highly available. This project brings Ardomus more the 10 million dollars of income.`);
        this.writeln('');
        this.writeln(`Last year, I built a team of 8 and won 2nd place in AWS Hack for Good hackathon. I was responsible to manage the schedule, design the software architecture and present our project on the final pitch. Last but not least, our winning project is open source and can be found on GitHub.`);
        break;
      case 'portfolio':
        this.writeln('');
        this.writeln(`Yeelight.io - https://www.npmjs.com/package/yeelight.io`);
        this.writeln(`• yeelight.io is a simple library for you to control`);
        this.writeln(`  YeeLight LED bulb through LAN`);
        this.writeln(`• 600+ download on NPM`);
        this.writeln(`• Used by 2 other open-source projects`);

        this.writeln('');
        this.writeln(`Hikoo - https://hikoo-team.github.io/hikoo`);
        this.writeln(`• Hikoo platform is management platform for the government `);
        this.writeln(`  to better track and protect the hikers`);

        this.writeln('');
        this.writeln(`Modbus Master Tool - https://github.com/tonylin0826/ModbusMasterTool`);
        this.writeln(`• A Modbus TCP Master Tool made using Qt`);

        this.writeln('');
        this.writeln(`http.io.c - https://github.com/tonylin0826/yeelight.io.c`);
        this.writeln(`• A C HTTP REST framework`);
        break;
      case 'resume':
        window.open('https://app.enhancv.com/share/96f104d4?utm_medium=growth&utm_campaign=share-resume&utm_source=dynamic');
        break;
      case 'links':
        this.writeln('Click the url to open link');
        this.writeln('Github:\t\thttps://tonylin0826.github.io');
        this.writeln('NPM:\t\thttps://www.npmjs.com/package/yeelight.io');
        this.writeln('Linkedin:\thttps://www.linkedin.com/in/tonylintw/');
        break;
      default:
        this.write(`command not found: '${this.textBuffer}'`);
        break;
    }
    this.textBuffer = '';
  }

  private _putToBuffer(char: string): void {
    this.textBuffer += char;
  }

  private _putBackspace(): void {
    this.textBuffer = this.textBuffer.slice(0, this.textBuffer.length - 1);
  }
}

@Component({
  // encapsulation: ViewEncapsulation.None,
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('terminal') terminalElement: ElementRef;
  @ViewChild('tooltipTerminal') tooltipTerminal: MatTooltip;

  faTerminal = faTerminal;
  faGithub = faGithub;
  faNpm = faNpm;
  faLinkedin = faLinkedin;
  faStackOverflow = faStackOverflow;

  t = false;
  o = false;
  n = false;
  y = false;
  l = false;
  i = false;
  nn = false;

  cl = undefined;

  terminal = new MyTerm();
  terminalOn = false;

  constructor(private ref: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.cl = setInterval(() => {
      ['t', 'o', 'n', 'y', 'l', 'i', 'nn'].forEach(p => {
        this[p] = true;
        setTimeout(() => {
          this[p] = false;
        }, 800);
      });
    }, 5000);

  }

  ngOnDestroy(): void {
    clearInterval(this.cl);
  }

  ngAfterViewInit(): void {
    console.log(this.terminalElement);
    if (this.tooltipTerminal) {
      // this.tooltipTerminal
      console.log(this.tooltipTerminal);
      this.tooltipTerminal.show();
    }
  }

  openGithub(): void {
    window.open('https://github.com/tonylin0826');
  }

  openNpm(): void {
    window.open('https://www.npmjs.com/package/yeelight.io');
  }

  openLinkedIn(): void {
    window.open('https://www.linkedin.com/in/tony-lin-b18b29ba');
  }

  openStackOverflow(): void {
    window.open('https://stackoverflow.com/users/2279854/tony-lin');
  }

  toggleTerminal(): void {
    this.terminalOn = !this.terminalOn;
    this.ref.detectChanges();

    if (!this.terminalOn) {
      this.terminal.dispose();
    } else {
      this.terminal = new MyTerm();
      this.terminal.open(this.terminalElement.nativeElement);
      this.terminal.focus();
      this.terminal.init();
    }
  }
}
